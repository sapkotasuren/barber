import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterBarberComponent } from './barbers/register-barber/register-barber.component';
import { CreateAppointmentComponent } from './barbers/create-appointment/create-appointment.component';
import { BarberLocationComponent } from './barbers/barber-location/barber-location.component';
import { CreateBarberServicesComponent } from './barbers/barber-service-overview/create-barber-services/create-barber-services.component';
import { BarberListComponent } from './barbers/barber-list/barber-list.component';
import { BarberDetailsComponent } from './barbers/barber-details/barber-details.component';
import { CreateOpeninghourComponent } from './barbers/barber-openingshour/create-openinghour/create-openinghour.component';
import { BarberServiceOverviewComponent } from './barbers/barber-service-overview/barber-service-overview.component';
import { BarberOpeningshoursListComponent } from './barbers/barber-openingshour/barber-openingshour-list.component';


const routes: Routes = [
  {
    path: 'registerBarber',
    component: RegisterBarberComponent,
  },
  {
    path: 'appointment',
    component: CreateAppointmentComponent,
  },
  {
    path: 'barberLocation',
    component: BarberLocationComponent,
  },
  {
    path: 'barberService',
    component: CreateBarberServicesComponent,
  },
  {
    path: 'barbersList',
    component: BarberListComponent
  },
  {
    path: 'barberDetails',
    component: BarberDetailsComponent, children: [
      {path: ':id', component: RegisterBarberComponent},
      {path: ':id/barberServiceOverview', component: BarberServiceOverviewComponent}, 
      {path: ':id/location', component: BarberLocationComponent},
      {path: ':id/openingsuren', component: CreateOpeninghourComponent},
      {path: ':id/appointment', component: CreateAppointmentComponent},
      {path: ':id/listBarberMayRemoved', component: BarberOpeningshoursListComponent},
      //{path: ':id/service', component: CreateBarberServicesComponent},
    ],
  },
  { path: '', redirectTo: '/barbersList', pathMatch: 'full' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
