export class Location {
    barberLocationId: number;
    street: string;
    houseNumber: string;
    bus: string;
    postCode: string;
    city: string;
    country: string;
    barberId: number;
}