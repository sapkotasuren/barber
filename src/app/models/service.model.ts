export class Service {
  barberServiceId: number;
  name: string;
  description: string;
  price: number
  gender: string;
  barberId: number;
}
