export class Barber{
  barberId: number;
  name: string;
  description: string;
  isMen: boolean;
  isWomen: boolean;
}
