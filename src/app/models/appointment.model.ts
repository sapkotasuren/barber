import { Time } from '@angular/common';

export class Appointment {
appointmentId: number;
name: string;
email: string;
phone: string;
gender: string;
time: string;
date: Date;
barberId: number;
serviceId: number;
}


/**
 * 
 *names: string;
  emails: string;
  phones: string;
  contactPreferences: string;
  genders: string;
  times: string;
  appointmentDate: string;
  services: string;
 */