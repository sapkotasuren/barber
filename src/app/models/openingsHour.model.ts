export class OpeningsHour {
    openingsHourId:number;
    day: string;
    m_from: Date;
    m_till: Date;
    a_from: Date = null;
    a_till: Date = null;
    isclose: boolean;
    barberId: number;
}