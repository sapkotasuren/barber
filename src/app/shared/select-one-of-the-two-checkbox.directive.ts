import { Directive } from "@angular/core";
import { NG_VALIDATORS, Validator, AbstractControl } from '@angular/forms';

@Directive({
  selector: '[appSelectOneOfTheTwoCheckbox]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: SelectOneOfTheTwoCheckbox,
      multi: true,
    },
  ],
})

export class SelectOneOfTheTwoCheckbox implements Validator {
  validate(barberGenderGroup: AbstractControl): { [key: string]: any} | null {
    const isMen = barberGenderGroup.get('men');
    const isWomen = barberGenderGroup.get('women');

    if (isMen?.value === false && isWomen?.value === false) {
      console.log("came here!");
      return { notSelected: true };
    }
    return null;
  }
}
