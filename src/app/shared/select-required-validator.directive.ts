import { Validator, AbstractControl, NG_VALIDATORS } from '@angular/forms';
import { Directive, Input } from '@angular/core';

@Directive({
  selector: '[appTimeSelectorValidator]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: SelectRequiredValidatorDirective,
    multi: true
  }]
})

export class SelectRequiredValidatorDirective implements Validator {
  //if success return null else object {}
  //what we pass from html = comes here!
  //control will have the ngmodel of the input field wher you have put the apptimeselectorValidator
  @Input('appTimeSelectorValidator') defaultValue: string;
  validate(control: AbstractControl): { [key: string]: any } | null {
    return control.value === this.defaultValue ? {defaultSelect: true} : null;
  }
}
