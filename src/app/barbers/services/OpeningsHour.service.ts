import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { OpeningsHour } from 'src/app/models/openingsHour.model';
import { environment } from 'src/environments/environment';
import { delay, catchError } from 'rxjs/operators';
import { Observable, of, throwError, Subject } from 'rxjs';
import { env } from 'process';

@Injectable({
    providedIn: 'root'
})
export class OpeningsHourService {
    private httpClient: HttpClient;

    updateOpeningsHourList = new Subject<OpeningsHour>();
    addedNewOpeningsHourInList = new Subject<OpeningsHour>();

    constructor(httpc: HttpClient) {
        this.httpClient = httpc;
    }

    getOpeningsHours(barberId: Number): Observable<OpeningsHour[]> {
        return this.httpClient
        .get<OpeningsHour[]>(environment.apiUrl+"barber/"+barberId+"/openingsHour")
        .pipe(catchError(this.handleError));
    }

    saveOpeningHour(barberId: Number, openingsHour: OpeningsHour): Observable<OpeningsHour> {
        return this.httpClient
        .post<OpeningsHour>(environment.apiUrl+"barber/"+barberId+"/openingsHour", openingsHour)
        .pipe(catchError(this.handleError));
    }

    updateOpeningHour(barberId: Number, openingsHour: OpeningsHour): Observable<OpeningsHour> {
        return this.httpClient
        .put<OpeningsHour>(environment.apiUrl+"barber/"+barberId+"/openingsHour", openingsHour)
        .pipe(catchError(this.handleError));
    }

    getOpeningsHourById(barberId: Number, openingsHourId: Number): Observable<OpeningsHour> {
        return this.httpClient
        .get<OpeningsHour>(environment.apiUrl+"barber/"+barberId+"/openingsHour/"+openingsHourId)
        .pipe(catchError(this.handleError));
    }

    deleteOpeningHourById(openingsHourId: Number): Observable<OpeningsHour> {
        return null;
    }


    

    private handleError(errorResponse: HttpErrorResponse) {
        if (errorResponse.error instanceof ErrorEvent) {
          console.error('Client side Error: ', errorResponse.error.message);
        } else {
          console.error('Server side error: ', errorResponse);
        }
        return throwError(
          'There is a problem with service, we are notified and we are working on it. Please try again later!'
        );
      }
}