import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Appointment } from 'src/app/models/appointment.model';
import { delay, catchError } from 'rxjs/operators';
import { Observable, of, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RegisterAppointmentService {
  private httpClient: HttpClient;

  constructor(httpc: HttpClient) {
    this.httpClient = httpc;
  }

  registerAppointment(appointment: Appointment):Observable<Appointment> {
    return this.httpClient
      .post<Appointment>(environment.apiUrl+"appointment", appointment,
        {
          headers: new HttpHeaders({
            'content-type': 'application/json',
          })
        }).pipe(catchError(this.handleError));
  }

  private handleError(errorResponse: HttpErrorResponse) {
    if (errorResponse.error instanceof ErrorEvent) {
      console.error('Client side Error: ', errorResponse.error.message);
    } else {
      console.error('Server side error: ', errorResponse);
    }
    return throwError(
      'There is a problem with service, we are notified and we are working on it. Please try again later!'
    );
  }
}