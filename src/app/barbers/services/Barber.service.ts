import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Barber } from 'src/app/models/barber.model';
import { delay, catchError } from 'rxjs/operators';
import { Observable, of, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BarberService {
  private httpClient: HttpClient;

  constructor(httpc: HttpClient) {
    this.httpClient = httpc;
  }

  getBarbers(): Observable<Barber[]> {
       return this.httpClient
        .get<Barber[]>(environment.apiUrl+"barber/all")
        .pipe(catchError(this.handleError));
  }

  addBarber(barber: Barber): Observable<Barber> {
    return this.httpClient.post<Barber>(environment.apiUrl+"barber", barber)
    .pipe(catchError(this.handleError));
  }

  getBarber(id: number): Observable<Barber> {
    return this.httpClient.get<Barber>(environment.apiUrl+"barber/"+id)
    .pipe(catchError(this.handleError));
  } 

  updateBarber(barber: Barber): Observable<Barber> {
    return this.httpClient.put<Barber>(environment.apiUrl+"barber", barber)
    .pipe(catchError(this.handleError));
  }

  deleteBarber(id: number): Observable<void> {
    return this.httpClient.delete<void>(environment.apiUrl+"barber/"+id)
    .pipe(catchError(this.handleError));
  }

  private handleError(errorResponse: HttpErrorResponse) {
    if (errorResponse.error instanceof ErrorEvent) {
      console.error('Client side Error: ', errorResponse.error.message);
    } else {
      console.error('Server side error: ', errorResponse);
    }
    return throwError(
      'There is a problem with service, we are notified and we are working on it. Please try again later!'
    );
  }
}