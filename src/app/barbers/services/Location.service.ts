import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Location } from 'src/app/models/location.model';
import { environment } from 'src/environments/environment';
import { delay, catchError } from 'rxjs/operators';
import { Observable, of, throwError } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class LocationService {
    private httpClient: HttpClient;

    constructor(httpc: HttpClient) {
        this.httpClient = httpc;
    }

    getLocation(barberId: number, locationId: number): Observable<Location> {
        if (locationId == null || locationId <= 0) {
            return this.httpClient.get<Location>(environment.apiUrl + "barber/" + barberId + "/location")
                .pipe(catchError(this.handleError));
        } else {
            return this.httpClient.get<Location>(environment.apiUrl + "barber/" + barberId + "/location/" + locationId)
                .pipe(catchError(this.handleError));
        }
    }

    saveLocation(barberId: number, barberLocation: Location): Observable<Location> {
        return this.httpClient.post<Location>(environment.apiUrl + "barber/" + barberId + "/location", barberLocation)
        .pipe(catchError(this.handleError));
    }

    updateLocation(barberId: number, barberLocation: Location): Observable<Location> {
        return this.httpClient.put<Location>(environment.apiUrl + "barber/" + barberId + "/location", barberLocation)
        .pipe(catchError(this.handleError));
    }

    private handleError(errorResponse: HttpErrorResponse) {
        if (errorResponse.error instanceof ErrorEvent) {
            console.error('Client side Error: ', errorResponse.error.message);
        } else {
            console.error('Server side error: ', errorResponse);
        }
        return throwError(
            'There is a problem with service, we are notified and we are working on it. Please try again later!'
        );
    }
}