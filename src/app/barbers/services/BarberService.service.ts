import {  Injectable, EventEmitter } from "@angular/core";
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { BarberService } from './Barber.service';
import { environment } from 'src/environments/environment';
import { delay, catchError } from 'rxjs/operators';
import { Observable, of, throwError, Subject } from 'rxjs';
import { Service } from 'src/app/models/service.model';
@Injectable({
    providedIn: 'root'
})

export class BarberServiceService {

    private httpClient: HttpClient;

    updateBarberServicesList = new Subject<Service>();
    addedNewBarberServicesInList = new Subject<Service>();

    constructor(httpc: HttpClient){
        this.httpClient = httpc;
    }

     getBarberService(barberId: number): Observable<Service[]> {
        return this.httpClient
        .get<Service[]>(environment.apiUrl+"barber/"+barberId + "/service")
        .pipe(catchError(this.handleError));
    }

    saveBarberService(barberId: number, barberService: Service) {
        return this.httpClient
        .post<Service>(environment.apiUrl+"barber/"+barberId+"/service", barberService)
        .pipe(catchError(this.handleError));
    }

    updateBarberService(barberId: number, barberService: Service) {
        return this.httpClient
        .put<Service>(environment.apiUrl+"barber/"+barberId+"/service", barberService)
        .pipe(catchError(this.handleError));
    }

    getBarberServiceByBarberIdAndBarberServiceId(barberId: number, barberServiceId: number): Observable<Service> {
        return this.httpClient
        .get<Service>(environment.apiUrl+"barber/"+barberId + "/service/"+barberServiceId)
        .pipe(catchError(this.handleError));
    }

    deleteBarberServiceByBarberServiceId(barberId: number, barberServiceId: number): Observable<void> {
        return this.httpClient
        .delete<void>(environment.apiUrl+"barber/"+barberId + "/service/"+barberServiceId)
        .pipe(catchError(this.handleError));
    }

    private handleError(errorResponse: HttpErrorResponse) {
        if (errorResponse.error instanceof ErrorEvent) {
            console.error('Client side Error: ', errorResponse.error.message);
        } else {
            console.error('Server side error: ', errorResponse);
        }
        return throwError(
            'There is a problem with service, we are notified and we are working on it. Please try again later!'
        );
    }
}