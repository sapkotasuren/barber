import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Barber } from 'src/app/models/barber.model';
import { delay, catchError } from 'rxjs/operators';
import { Observable, of, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RegisterBarberService {

  //baseUrl: string = 'http://localhost:8080/barber';

  private httpClient: HttpClient;

  constructor(httpC: HttpClient) {
    this.httpClient = httpC;
  }

  registerBarber(barber: Barber): Observable<Barber>{
    return this.httpClient
            .post<Barber>(environment.apiUrl+"barber", barber,
              {
                headers: new HttpHeaders({
                  'content-type': 'application/json',
                })
              }).pipe(catchError(this.handleError));
              /*.subscribe(
              resp => {
                console.log("successfully sent the request and received the response");
              },
              (error: any) => {
                this.handleError(error);
              },
              () => {
                console.log("The post observable method is now completed");
              });*/
  }

  private handleError(errorResponse: HttpErrorResponse) {
    if (errorResponse.error instanceof ErrorEvent) {
      console.error('Client side Error: ', errorResponse.error.message);
    } else {
      console.error('Server side error: ', errorResponse);
    }
    return throwError(
      'There is a problem with service, we are notified and we are working on it. Please try again later!'
    );
  }
}

