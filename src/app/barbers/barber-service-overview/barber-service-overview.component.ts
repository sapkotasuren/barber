import { Component, OnInit, Output } from '@angular/core';
import { Service } from 'src/app/models/service.model';
import {  Router, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { stat } from 'fs';

@Component({
  selector: 'app-barber-service-overview',
  templateUrl: './barber-service-overview.component.html',
  styleUrls: ['./barber-service-overview.component.css']
})
export class BarberServiceOverviewComponent implements OnInit {

  serviceForEdit: Service;
  barberId:number;
  
  constructor(private route: ActivatedRoute) {}

  ngOnInit(): void {
      this.route.paramMap.subscribe(params => {
        this.barberId = +params.get('id');
        console.log("barber service overview, barberId -> " +params.get('id'));
      });
  }

  editBarberService(selectedServiceForEdit: Service):void {
    this.serviceForEdit = selectedServiceForEdit;
  }

}
