import { Component, OnInit, Output, EventEmitter, OnDestroy } from '@angular/core';
import { Service } from 'src/app/models/service.model';
import { BarberServiceService } from '../../services/BarberService.service';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-barber-service-list',
  templateUrl: './barber-service-list.component.html',
  styleUrls: ['./barber-service-list.component.css']
})
export class BarberServiceListComponent implements OnInit, OnDestroy {

  subscription: Subscription;

  @Output() selectedServiceForEdit: EventEmitter<Service> = new EventEmitter<Service>();

  barberId: number;
  services: Service[] = [];

  constructor(private route: ActivatedRoute, private barberServiceService: BarberServiceService) { }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.barberId = +params.get('id');
      console.log("create barber service component, barberId -> " + params.get('id'));
    });
    this.subscription = this.barberServiceService.updateBarberServicesList
      .subscribe(
        (service: Service) => {
          console.table("barberservice list: " + service.name);
          this.replaceService(service);
        });
     this.subscription = this.barberServiceService.addedNewBarberServicesInList.subscribe(
       (service: Service)=> {
         this.addService(service);
       }
     )   
    this.getBarberServices();
  }

  getBarberServices() {
    this.barberServiceService.getBarberService(this.barberId).
      subscribe((val) => {
        this.services = val;
      },
        (error) => {
          console.log("error occured");
        },
        () => {
          console.log("execution completed!");
        });
  }


  editService(service: Service) {
    //pass here the copy of the service, otherwise if you pass the orignal one than while editing in form it changes constantly...
    let copyService: Service = Object.assign({}, service);
    this.selectedServiceForEdit.emit(copyService);
  }

  deleteBarberService(service: Service) {
    this.barberServiceService.deleteBarberServiceByBarberServiceId(service.barberId, service.barberServiceId)
      .subscribe((val) => {
      },
        (error) => {
          console.log("error occured in barber-service-list.component.ts");
        },
        () => {
          this.getBarberServices()
        });
  }


  private replaceService(service: Service): void {
    if (this.services != null && this.services.length > 0) {
      this.services.forEach((value, index) => {
        if (value.barberServiceId == service.barberServiceId) {
          this.services.splice(index, 1, service);
        }
      })
    }
  }

  private addService(service: Service) {
    this.services.push(service);
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
