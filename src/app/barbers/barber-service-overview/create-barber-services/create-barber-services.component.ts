import { Component, OnInit, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Service } from 'src/app/models/service.model';
import { BarberServiceService } from '../../services/BarberService.service';
import { ActivatedRoute } from '@angular/router';
import { format } from 'path';

@Component({
  selector: 'app-create-barber-services',
  templateUrl: './create-barber-services.component.html',
  styleUrls: ['./create-barber-services.component.css']
})
export class CreateBarberServicesComponent implements OnInit {

  @Input() service: Service = null;

  showForm: boolean = false;

  barberId: number;

  constructor(private route: ActivatedRoute, private barberServiceService: BarberServiceService) {
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.barberId = +params.get('id');
      console.log("create barber service component, barberId -> " + params.get('id'));
    });
  }

  ngOnChanges() {
    /** When the data/object from service-overview then we want form to open automatically.*/
    if (this.service != null) {
      this.showForm = true;
    }
  }

  showOrHideForm() {
    this.showForm = !this.showForm;
  }

  registerBarberService(form: NgForm) {
    if (this.service.barberServiceId != -1) {
      this.updateBarberServiceService();
    } else {
      this.registerBarberServiceService();
    }
    this.initializeEmptyObject();
  }

  private registerBarberServiceService(): void {
    let savedService: Service;
    this.barberServiceService.saveBarberService(this.barberId, this.service).
      subscribe(val => {    
        savedService = val;    
        console.log("registerd new service");
      },
      (error) =>{

      },
      () => {
        this.barberServiceService.addedNewBarberServicesInList.next(savedService);
      });
  }

  private updateBarberServiceService(): void {
    let savedService: Service;
    this.barberServiceService.updateBarberService(this.barberId, this.service).
      subscribe(val => {
        savedService = val;
      },
        (error) => {

        },
        () => {
          console.log("going to emit it");
          this.barberServiceService.updateBarberServicesList.next(savedService);
        });
  }

  initializeEmptyObject() {
    this.service = {
      barberServiceId: -1,
      name: '',
      description: '',
      price: null,
      gender: '',
      barberId: this.barberId
    }
    this.showOrHideForm();
  }


}
