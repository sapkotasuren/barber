import { Component, OnInit } from '@angular/core';
import { BarberService } from '../services/Barber.service';
import { Barber} from '../../models/barber.model';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-barber-list',
  templateUrl: './barber-list.component.html',
  styleUrls: ['./barber-list.component.css']
})
export class BarberListComponent implements OnInit {

  barbers: Barber[];
  constructor(private barberService: BarberService, private router: Router) {}

  ngOnInit(): void {
    this.barberService.getBarbers().subscribe((val) => {
      this.barbers = val;
    });
  }

  deleteBarber(id: number) {
    console.log(id);
    this.barberService.deleteBarber(id).
    subscribe(val => {
      console.log("Successfully sended and its been deleted"+ val);
    },
    (error => {
      console.log("error occured?");
    }),
    () => {
      console.log("complete of deletion");
    }
    )
  }


  viewDetails(barberId: number) {
    this.router.navigate(['barberDetails', barberId]);
  }

}
