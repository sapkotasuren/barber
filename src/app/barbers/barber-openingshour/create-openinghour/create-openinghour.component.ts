import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
import { OpeningsHour } from 'src/app/models/openingsHour.model';
import { ActivatedRoute } from '@angular/router';
import { OpeningsHourService } from '../../services/OpeningsHour.service';

@Component({
  selector: 'app-create-openinghour',
  templateUrl: './create-openinghour.component.html',
  styleUrls: ['./create-openinghour.component.css']
})
export class CreateOpeninghourComponent implements OnInit, OnChanges {

  @Input() editOpeninghour: OpeningsHour;
  barberId: number;
  isMeridian: boolean = false;
  weekdays: string[] = ["Maandag", "Dinsdag", "Woensdag", "Donderdag", "Vrijdag", "Zaterdag", "Zondag"];
  openingsHour: OpeningsHour;
  openingsHourList: OpeningsHour[] = [];
  // names:string="";
  // gesloten:boolean = false;
  // selectedDay:string;
  // openVan: Date = new Date();
  // openTot: Date = new Date();
  
  constructor(private openingHourService: OpeningsHourService, private route: ActivatedRoute) {
    this.initializeOpeninghoursObject();
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      this.barberId = +params.get('id');
      console.log("create openingshour component, barberId -> " + params.get('id'));
      this.openingsHour.barberId = this.barberId;
    });
    this.getAllOpeningsHours();
  }

  ngOnChanges(): void {
    if (this.editOpeninghour != null && this.editOpeninghour.openingsHourId > 0 && this.editOpeninghour.barberId > 0) {
      this.openingsHour = this.editOpeninghour;
    }
  }

  onSubmit(): void {
    if (this.openingsHour.openingsHourId === -1){
      this.addOpeningsHour(this.barberId, this.openingsHour);
    } else if (this.openingsHour.openingsHourId > 0) {
      this.updateOpeningsHour(this.barberId, this.openingsHour);
    } 
    //send to the server and notify the subscriber
  }

  addOpeningsHour(barberId:number, openingsHour: OpeningsHour): void {
    let savedOpeningHour = null;
    this.openingHourService.saveOpeningHour(this.barberId, this.openingsHour)
    .subscribe(val => {
      savedOpeningHour = val;
      console.log("registerd new openingshourd: " + val);
    },
      (error) => {
        console.log("Error occured while saving the openingsuren: " + error);
      },
      () => {
        this.getAllOpeningsHours();
        this.openingHourService.addedNewOpeningsHourInList.next(savedOpeningHour);
        console.log("method completion");
      });
    this.initializeOpeninghoursObject();
  }

  updateOpeningsHour(barberId:number, openinghour: OpeningsHour): void {
    let updatedOpeningHour;
    this.openingHourService.updateOpeningHour(this.barberId,openinghour)
    .subscribe(val => {
      updatedOpeningHour = val;
      console.log("updated openingshour: "+val.openingsHourId);
    },
    (error) => {
      console.log("Error occured while saving the openingsuren: " + error);
    },
    () => {
      console.log("method completion");
      this.getAllOpeningsHours();
      this.openingHourService.updateOpeningsHourList.next(updatedOpeningHour);
    });
    this.initializeOpeninghoursObject();
  }

  sluitingDag(): void {
    this.openingsHour.isclose = !this.openingsHour.isclose;
  }

  initializeOpeninghoursObject() {
    this.openingsHour = {
      openingsHourId: -1,
      day: null,
      m_from: new Date(),
      m_till: new Date(),
      a_from: null,
      a_till: null,
      isclose: false,
      barberId: this.barberId,
    };
  }

  removeDaysFromWeek() {
    /*for (let i = 0; i < this.weekdays.length; i++) {
      for (let j = 0; j < this.openingsHourList.length; j++) {
        if (this.weekdays[i]===this.openingsHourList[j].day){
          this.weekdays.splice(i,1);
        }
      }
    }*/
    this.openingsHourList.forEach((val)=>{
      const i = this.weekdays.indexOf(val.day,0);
      if (i > -1) {
        this.weekdays.splice(i,1);
      }
    })

  }

  getAllOpeningsHours() {
    // this method is called each time when save method is called because to know which day it should show in dropdown.
    this.openingHourService.getOpeningsHours(this.barberId)
      .subscribe(val => {
        this.openingsHourList = val;
      },
      (error) => {
        console.log("Error occured while saving the openingsuren: "+ error);
      },
      () => {
        console.log("method completion");
        this.removeDaysFromWeek();
      });
    }
}
