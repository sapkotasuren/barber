import { Component, OnInit, Input, OnChanges, OnDestroy } from '@angular/core';
import { OpeningsHour } from 'src/app/models/openingsHour.model';
import { OpeningsHourService } from '../services/OpeningsHour.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-barber-openingshour-list',
  templateUrl: './barber-openingshour-list.component.html',
  styleUrls: ['./barber-openingshour-list.component.css']
})
export class BarberOpeningshoursListComponent implements OnInit, OnDestroy {

  private barberId: number;
  openingHour: OpeningsHour;
  OpeningsHours: OpeningsHour[] = [];
  subscription: Subscription;
  /**
        "openingsHourId": 1,
        "day": "Maandag",
        "m_from": new Date(),
        "m_till": new Date(),
        "a_from": null,
        "a_till": null,
        "isclose": false,
        "barberId": 5
      },
      {
        "openingsHourId": 2,
        "day": "Dinsdag",
        "m_from": new Date(),
        "m_till": new Date(),
        "a_from": null,
        "a_till": null,
        "isclose": false,
        "barberId": 5
      }
   */
  constructor(private openingsHourService: OpeningsHourService, private route: ActivatedRoute) { }

  ngOnInit(): void {
    //fetch barbeId from url;
    this.route.paramMap.subscribe(params => {
      this.barberId = +params.get('id');
      console.log("barber openingshours list, barberId: " + this.barberId);
    });
    //fetch all data
    this.getAllOpeningsHoursData();

    this.subscription = this.openingsHourService.addedNewOpeningsHourInList
                        .subscribe(val => {
                          this.OpeningsHours.push(val);
                        });
    this.subscription = this.openingsHourService.updateOpeningsHourList
                        .subscribe(val => {
                          this.replaceOpeningsHour(val);
                        });                  

  }

  editService(openingHour: OpeningsHour): void {
    if (openingHour != null && openingHour.openingsHourId > 0 && openingHour.barberId > 0) {
      this.openingHour = {...openingHour};
    }
  }

  getAllOpeningsHoursData() {
    if (this.barberId != null && this.barberId > 0) {
      this.openingsHourService.getOpeningsHours(this.barberId)
        .subscribe(val => {
          this.OpeningsHours = val;
          console.table(val);
        },
          (error) => {
            console.log("Error occured while saving the openingsuren: " + error);
          },
          () => {
            console.log("method completion");
          });
    }
  }

  replaceOpeningsHour(openingHour: OpeningsHour): void {
    this.OpeningsHours.forEach((e, index)=> {
      if (e.openingsHourId == openingHour.openingsHourId) {
        this.OpeningsHours[index] = openingHour;
      }
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
