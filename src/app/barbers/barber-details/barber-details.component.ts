import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-barber-details',
  templateUrl: './barber-details.component.html',
  styleUrls: ['./barber-details.component.css']
})
export class BarberDetailsComponent implements OnInit {

  barberId: number;
  constructor(private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit(): void {
      this.barberId = +this.route.firstChild.snapshot.params['id'];
  }

  navigate(destination: string) {
      if (destination === 'location') {
        this.router.navigate(['barberDetails/' + this.barberId + '/location']);
      } else if (destination === 'service') {
        this.router.navigate(['barberDetails/' + this.barberId + '/barberServiceOverview']);
      } else if (destination === 'openingsuren') {
        this.router.navigate(['barberDetails/' + this.barberId + '/openingsuren']);
      } else if (destination === 'maakAfspraak') {
        this.router.navigate(['barberDetails/' + this.barberId + '/appointment']);
      } else if (destination === 'kapperDetail') {
        this.router.navigate(['barberDetails/', this.barberId]);
      } else if (destination === 'mayRemoved') {
        this.router.navigate(['barberDetails/'+ this.barberId + '/listBarberMayRemoved']);
      }
  }

}
