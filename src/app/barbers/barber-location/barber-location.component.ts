import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LocationService } from '../services/Location.service';
import { Location } from '../../models/location.model';
import { ActivatedRoute, Params } from '@angular/router';
import { error } from 'protractor';

@Component({
  selector: 'app-barber-location',
  templateUrl: './barber-location.component.html',
  styleUrls: ['./barber-location.component.css']
})
export class BarberLocationComponent implements OnInit {

  inputFieldDisable: boolean = false;

  location: Location = {
    barberLocationId: -1,
    street: null,
    houseNumber: null,
    bus: null,
    postCode: null,
    city: null,
    country: "België",
    barberId: -1
  }

  constructor(private locationService: LocationService, private route: ActivatedRoute) {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.location.barberId = +params['id'];
        }
      );
  }

  ngOnInit(): void {
    if (this.location.barberId != null && this.location.barberId > 0) {
    this.getBarberLocation();
    }
  }

  saveBarberLocation(locatieBarberForm: NgForm) {
    if (this.location.barberId > -1 && (this.location.barberLocationId == -1 || this.location.barberLocationId == null)) {
      this.registerBarberLocation();
    } else if (this.location.barberId > -1 && this.location.barberLocationId > 0) {
      this.updateBarberLocation();
    }
  }

  private getBarberLocation(): void {
    this.locationService.getLocation(this.location.barberId, null).
    subscribe(val => {
      this.location = val;
    },
    (error => {
      console.log("Error occured");
    }),
    () => {
      this.disableInputField();
      console.log("Method execution completed getLocation");
    });
  } 


  private registerBarberLocation(): void{
    this.locationService.saveLocation(this.location.barberId, this.location).
    subscribe(val => {
      this.location = val;
      console.log("Successfully sended to the server and the response is en de barberId is: " + val.barberId + " locationId:" + val.barberLocationId);
    },
      (error => {
        console.log("Error occured?");
      }),
      () => {
        this.disableInputField();
        console.log("saveLocation method execution completed");
      }
    );
  }

  private updateBarberLocation(): void {
    this.locationService.updateLocation(this.location.barberId, this.location).
    subscribe(val => {
      this.location = val;
      console.log("Successfully sended to the server and the response is en de barberId is: " + val.barberId + " locationId:" + val.barberLocationId);
    },
      (error => {
        console.log("Error occured?");
      }),
      () => {
        this.disableInputField();
        console.log("saveLocation method execution completed");
      }
    );
  }

  onEdit(): void {
    this.inputFieldDisable = !this.inputFieldDisable;
  }

  private disableInputField(): void {
    (this.location.barberLocationId > 0)? this.inputFieldDisable = true:this.inputFieldDisable = false;
  }

}
