import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Hour } from 'src/app/models/hour.model';
import { Service } from 'src/app/models/service.model';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker/public_api';
import { DatePipe } from '@angular/common';
import { Appointment } from 'src/app/models/appointment.model';

@Component({
  selector: 'app-create-appointment',
  templateUrl: './create-appointment.component.html',
  styleUrls: ['./create-appointment.component.css']
})
export class CreateAppointmentComponent implements OnInit {

  datePickerConfig: Partial<BsDatepickerConfig>;
  maxDate: Date = new Date();

  names: string;
  emails: string;
  phones: string;
  contactPreferences: string;
  genders: string;
  times: string;
  appointmentDate: string;
  services: string;

/**above object should replaced with the belowed one in html */
  appointment: Appointment = {
    appointmentId: -1,
    name: null,
    email: null,
    phone: null,
    gender: null,
    time: null,
    date: new Date(),
    barberId: -1,
    serviceId: -1
  };

  availableHours: Hour[] = [
    { id: 1, time: '10:00' },
    { id: 2, time: '10:30' },
    { id: 3, time: '11:00' },
    { id: 4, time: '11:30' },
    { id: 5, time: '12:00' },
    { id: 6, time: '12:30' },
    { id: 7, time: '13:00' },
  ];

  barberService: Service[] = [
    { barberServiceId: 1, name: 'Hair Cut',description:'', gender: 'F',price: 10.00, barberId:1},
    { barberServiceId: 2, name: 'Beard Trimmer',description:'', gender: 'M', price: 10.00, barberId:1},
    { barberServiceId: 3, name: 'Clean Shave',description:'', gender: 'M', price: 10.00, barberId: 1},
    { barberServiceId: 4, name: 'Hair Coloring',description:'', gender: 'F', price: 10.00, barberId: 1},
  ];

  constructor(private datePipe: DatePipe) {
  }

  ngOnInit(): void {
    this.maxDate.setTime(this.maxDate.getTime() + (600 * 24 * 60 * 60 * 1000));
    this.appointmentDate = this.getCurrentDate();
    this.datePickerConfig = Object.assign({},
    {
      containerClass: 'theme-dark-blue',
      dateInputFormat: 'DD/MM/YYYY',
      showWeekNumbers: false,
      minDate: new Date(),
      maxDate: this.maxDate,
      daysDisabled: [ 1 ],
    });
  }

  saveAppointment(appointment: NgForm) {
    console.log("testtesdfqdsqdsf");
    console.log(" test " + appointment.value);
    console.log(appointment.valid);
    console.log(appointment.value.name);
    console.log(appointment.value.appointmentDate);
    console.log("yeaeee "+this.datePipe.transform(appointment.value.appointmentDate, 'dd/MM/yyyy'));
    let date = new Date(this.datePipe.transform(appointment.value.appointmentDate, 'dd/MM/YYYY'));
    console.log(date);
    var time: string = null;
    this.availableHours.forEach(e => {
      if (e.id == appointment.value.time) {
        console.log(e.id + " "+e.time + "hehehe");
        time = e.time;
        console.log("--------"+time + "    ");
      }
    });
    var c = appointment.value.appointmentDate;
    var a = this.datePipe.transform(appointment.value.appointmentDate, 'dd/MM/yyyy').split("/");
    var b = time.split(":");
    let d = new Date();
    d.setFullYear(parseInt(a[2]));
    let month = parseInt(a[1]);
    console.log(a[1]+"month");
    if (month > 0) {
    d.setMonth(month-1);
    }else {
      d.setMonth(month);
    }
    d.setDate(parseInt(a[0]));
    d.setHours(parseInt(b[0]));
    d.setMinutes(parseInt(b[1]));
    console.log(d);
    appointment.resetForm();
    
    /*let a = date.toString().split("/");
    console.log(time+"---");
    let b = time.split(":");
    console.log(b[0]);
    let d = new Date(parseInt(a[2]),parseInt(a[1])-1,parseInt(a[0]),parseInt(b[0]),parseInt(b[1]),0,0);
    console.log(d);*/
  }

  private getCurrentDate(): string {
    var today = new Date();
    var dd = String(today.getDate()).padStart(2,'0'); 
    var mm = String(today.getMonth()+1).padStart(2,'0');
    var yyyy = today.getFullYear();
    return dd+'/'+mm+'/'+yyyy;
  }

}
