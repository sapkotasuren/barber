import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { RegisterBarberService } from '../services/RegisterBarber.service';
import { Barber } from 'src/app/models/barber.model';
import { error } from 'protractor';
import { BarberService } from '../services/Barber.service';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-register-barber',
  templateUrl: './register-barber.component.html',
  styleUrls: ['./register-barber.component.css']
})
export class RegisterBarberComponent implements OnInit {

  inputFieldDisable: boolean = false;

  barber: Barber = {
    barberId: -1,
    name: null,
    description: null,
    isMen: null,
    isWomen: null
  };

  constructor(private route: ActivatedRoute, private barberService: BarberService) {
  }

  ngOnInit(): void {
    this.route.paramMap.subscribe(params => {
      if (params.get('id') != null) {
        this.barber.barberId = +params.get('id');
        console.log("create barber service component, barberId -> " + params.get('id'));
      }
    });
    if (this.barber.barberId != null && this.barber.barberId >= 0) {
      this.getBarber();
    }
  }


  onSubmit(registerBarberForm: NgForm) {
    if (this.barber.barberId <= 0) {
      this.registerBarber();
    } else {
      this.updateBarber();
    }
    registerBarberForm.reset();
  }


  registerBarber() {
    this.barberService.addBarber(this.barber).
      subscribe(val => {
        console.log("successfully sended to the server and the response is en de barberId is: " + val.barberId);
      },
        (error => {
          console.log("Error occured?")
        }),
        () => {
          console.log("completed execution");
          this.getBarber();
        });
  }

  updateBarber() {
    this.barberService.updateBarber(this.barber)
      .subscribe((val) => {
      },
        (error) => {

        },
        () => {
          console.log("completed");
          this.getBarber();
        })
  }

  getBarber() {
    console.log("test");
    this.barberService.getBarber(this.barber.barberId)
      .subscribe(val => {
        this.barber = val;
      },
        (error) => {
          console.log("error occured");
        },
        () => {
          this.disableInputField();
          console.log("compled fetching barber");
        }
      )
  }

  onEdit(): void {
    this.inputFieldDisable = !this.inputFieldDisable;
  }

  private disableInputField(): void {
    /*(this.barber.barberId != null && this.barber.barberId > 0)
     this.inputFieldDisable = true
     this.inputFieldDisable = false;*/

    if (this.barber.barberId != null && this.barber.barberId > 0) {
      this.inputFieldDisable = true;
    } else {
      this.inputFieldDisable = false;
    }

  }

}
