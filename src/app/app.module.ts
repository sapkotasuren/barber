import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CreateAppointmentComponent } from './barbers/create-appointment/create-appointment.component';

import { HttpClientModule } from '@angular/common/http';

import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SelectRequiredValidatorDirective} from './shared/select-required-validator.directive';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { DatePipe } from '@angular/common';
import { RegisterBarberComponent } from './barbers/register-barber/register-barber.component'
import { SelectOneOfTheTwoCheckbox } from './shared/select-one-of-the-two-checkbox.directive';
import { RegisterBarberService } from './barbers/services/RegisterBarber.service';
import { BarberService } from './barbers/services/Barber.service';
import { LocationService } from './barbers/services/Location.service';
import { RegisterAppointmentService } from './barbers/services/RegisterAppointment.service';
import { BarberLocationComponent } from './barbers/barber-location/barber-location.component';
import { CreateBarberServicesComponent } from './barbers/barber-service-overview/create-barber-services/create-barber-services.component';
import { BarberListComponent } from './barbers/barber-list/barber-list.component';
import { BarberDetailsComponent } from './barbers/barber-details/barber-details.component';
import { BarberHeaderComponent } from './barbers/barber-header/barber-header.component';
import { BarberServiceListComponent } from './barbers/barber-service-overview/barber-service-list/barber-service-list.component';
import { BarberServiceOverviewComponent } from './barbers/barber-service-overview/barber-service-overview.component';
import { BarberServiceService } from './barbers/services/BarberService.service';
import { BarberOpeningshoursListComponent } from './barbers/barber-openingshour/barber-openingshour-list.component';
import { CreateOpeninghourComponent } from './barbers/barber-openingshour/create-openinghour/create-openinghour.component';
import { OpeningsHourService } from './barbers/services/OpeningsHour.service';


@NgModule({
  declarations: [
    AppComponent,
    CreateAppointmentComponent,
    SelectRequiredValidatorDirective,
    RegisterBarberComponent,
    SelectOneOfTheTwoCheckbox,
    BarberLocationComponent,
    CreateBarberServicesComponent,
    BarberListComponent,
    BarberDetailsComponent,
    BarberHeaderComponent,
    CreateOpeninghourComponent,
    BarberServiceListComponent,
    BarberServiceOverviewComponent,
    BarberOpeningshoursListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot()
  ],
  providers: [DatePipe, RegisterBarberService,RegisterAppointmentService, BarberService,LocationService,BarberServiceService, OpeningsHourService],
  bootstrap: [AppComponent]
})
export class AppModule { }
